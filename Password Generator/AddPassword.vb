﻿Public Class AddPassword
    Private Sub AddPassword_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        If Main.settings(1) <> "" And Main.settings(2) = True Then
            TextBox1.Enabled = True
            TextBox1.Text = ""
        Else
            TextBox1.Text = Main.settings(1)
            TextBox1.Enabled = False
        End If
        TextBox2.Text = ""
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If TextBox1.Text = Main.settings(1) Then
            If Button1.Tag = "GeneratedPassword" Then
                Dim p_had As Boolean = False
                For i As Integer = 0 To Main.journal.Length - 1
                    If Main.journal(i).Split("/")(0) = TextBox2.Text Then
                        p_had = True
                    End If
                Next
                If TextBox2.Text <> "" And TextBox2.Text.IndexOf("<") = -1 And TextBox2.Text.IndexOf(">") = -1 And TextBox2.Text.IndexOf("/") = -1 And TextBox2.Text.IndexOf("\") = -1 And p_had = False Then
                    Array.Resize(Main.journal, Main.journal.Length + 1)
                    Main.journal(Main.journal.Length - 1) = TextBox2.Text & "/" & Generator.TextBox1.Text
                    Main.Journal_Write()
                    Me.Close()
                Else
                    MsgBox("Имя содержит недопустимые символы!", , "Ошибка")
                End If
            Else
                Dim p_had As Boolean = False
                For i As Integer = 0 To Main.journal.Length - 1
                    If Main.journal(i).Split("/")(0) = TextBox2.Text Then
                        p_had = True
                    End If
                Next
                If TextBox2.Text <> "" And TextBox2.Text.IndexOf("<") = -1 And TextBox2.Text.IndexOf(">") = -1 And TextBox2.Text.IndexOf("/") = -1 And TextBox2.Text.IndexOf("\") = -1 And p_had = False Then
                    Array.Resize(Main.journal, Main.journal.Length + 1)
                    Main.journal(Main.journal.Length - 1) = TextBox2.Text & "/" & Generator.ListBox1.SelectedItem
                    Main.Journal_Write()
                    Me.Close()
                Else
                    MsgBox("Имя содержит недопустимые символы!", , "Ошибка")
                End If
            End If
        Else
            TextBox1.Text = ""
            MsgBox("Пароль введён неверно!", , "Ошибка")
        End If
    End Sub
End Class
