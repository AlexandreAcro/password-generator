﻿Public Class DoubleNames
    Private Sub DoubleNames_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        CheckBox1.Checked = False
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.DialogResult = DialogResult.Abort
        Me.Close()
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.DialogResult = DialogResult.Ignore
        Me.Close()
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.DialogResult = DialogResult.Retry
        Me.Close()
    End Sub
End Class
