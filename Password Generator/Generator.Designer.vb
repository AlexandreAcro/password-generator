﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Generator
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Generator))
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ContextMenuTextBoxPassword = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ОчиститьToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.КопироватьToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.СохранитьПарольВФайлToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.СохранитьПарольВЖурналToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ContextMenuListBox = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ОчиститьToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.СохранитьВФайлToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.СохранитьВЖурналToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.ContextMenuLivel = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.УровеньToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.УровеньToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.PasswordGenerator = New System.Windows.Forms.Timer(Me.components)
        Me.ContextMenuTextBoxPassword.SuspendLayout()
        Me.ContextMenuListBox.SuspendLayout()
        Me.ContextMenuLivel.SuspendLayout()
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox1.ContextMenuStrip = Me.ContextMenuTextBoxPassword
        Me.TextBox1.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(13, 159)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(416, 29)
        Me.TextBox1.TabIndex = 0
        '
        'ContextMenuTextBoxPassword
        '
        Me.ContextMenuTextBoxPassword.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.ContextMenuTextBoxPassword.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ОчиститьToolStripMenuItem1, Me.КопироватьToolStripMenuItem, Me.СохранитьПарольВФайлToolStripMenuItem, Me.СохранитьПарольВЖурналToolStripMenuItem})
        Me.ContextMenuTextBoxPassword.Name = "ContextMenuTextBoxPassword"
        Me.ContextMenuTextBoxPassword.ShowImageMargin = False
        Me.ContextMenuTextBoxPassword.Size = New System.Drawing.Size(222, 92)
        '
        'ОчиститьToolStripMenuItem1
        '
        Me.ОчиститьToolStripMenuItem1.Name = "ОчиститьToolStripMenuItem1"
        Me.ОчиститьToolStripMenuItem1.Size = New System.Drawing.Size(221, 22)
        Me.ОчиститьToolStripMenuItem1.Text = "Очистить"
        '
        'КопироватьToolStripMenuItem
        '
        Me.КопироватьToolStripMenuItem.Name = "КопироватьToolStripMenuItem"
        Me.КопироватьToolStripMenuItem.Size = New System.Drawing.Size(221, 22)
        Me.КопироватьToolStripMenuItem.Text = "Копировать"
        '
        'СохранитьПарольВФайлToolStripMenuItem
        '
        Me.СохранитьПарольВФайлToolStripMenuItem.Name = "СохранитьПарольВФайлToolStripMenuItem"
        Me.СохранитьПарольВФайлToolStripMenuItem.Size = New System.Drawing.Size(221, 22)
        Me.СохранитьПарольВФайлToolStripMenuItem.Text = "Сохранить пароль в файл"
        '
        'СохранитьПарольВЖурналToolStripMenuItem
        '
        Me.СохранитьПарольВЖурналToolStripMenuItem.Name = "СохранитьПарольВЖурналToolStripMenuItem"
        Me.СохранитьПарольВЖурналToolStripMenuItem.Size = New System.Drawing.Size(221, 22)
        Me.СохранитьПарольВЖурналToolStripMenuItem.Text = "Сохранить пароль в журнал"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(329, 194)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(100, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Закрыть"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 140)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(183, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Сгенерированый пароль:"
        '
        'ListBox1
        '
        Me.ListBox1.ContextMenuStrip = Me.ContextMenuListBox
        Me.ListBox1.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.HorizontalScrollbar = True
        Me.ListBox1.Location = New System.Drawing.Point(12, 39)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(184, 82)
        Me.ListBox1.TabIndex = 5
        '
        'ContextMenuListBox
        '
        Me.ContextMenuListBox.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ОчиститьToolStripMenuItem, Me.СохранитьВФайлToolStripMenuItem, Me.СохранитьВЖурналToolStripMenuItem})
        Me.ContextMenuListBox.Name = "ContextMenuListBox"
        Me.ContextMenuListBox.ShowImageMargin = False
        Me.ContextMenuListBox.Size = New System.Drawing.Size(173, 70)
        '
        'ОчиститьToolStripMenuItem
        '
        Me.ОчиститьToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.ОчиститьToolStripMenuItem.Name = "ОчиститьToolStripMenuItem"
        Me.ОчиститьToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.ОчиститьToolStripMenuItem.Text = "Очистить"
        '
        'СохранитьВФайлToolStripMenuItem
        '
        Me.СохранитьВФайлToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.СохранитьВФайлToolStripMenuItem.Name = "СохранитьВФайлToolStripMenuItem"
        Me.СохранитьВФайлToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.СохранитьВФайлToolStripMenuItem.Text = "Сохранить в файл"
        '
        'СохранитьВЖурналToolStripMenuItem
        '
        Me.СохранитьВЖурналToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.СохранитьВЖурналToolStripMenuItem.Name = "СохранитьВЖурналToolStripMenuItem"
        Me.СохранитьВЖурналToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.СохранитьВЖурналToolStripMenuItem.Text = "Сохранить в журнал"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(9, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(183, 27)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Пароли, сгенерированые за этот сеанс:"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(262, 30)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(85, 23)
        Me.Button4.TabIndex = 7
        Me.Button4.Text = "/\"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(262, 59)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(85, 23)
        Me.Button5.TabIndex = 8
        Me.Button5.Text = "\/"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(249, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(167, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Настройки генерации:"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(84, 191)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(238, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "-/-"
        '
        'TextBox2
        '
        Me.TextBox2.ContextMenuStrip = Me.ContextMenuLivel
        Me.TextBox2.Font = New System.Drawing.Font("Lucida Console", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(353, 30)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(55, 23)
        Me.TextBox2.TabIndex = 13
        Me.TextBox2.Text = "-"
        '
        'ContextMenuLivel
        '
        Me.ContextMenuLivel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.ContextMenuLivel.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.УровеньToolStripMenuItem, Me.УровеньToolStripMenuItem1})
        Me.ContextMenuLivel.Name = "ContextMenuLivel"
        Me.ContextMenuLivel.ShowImageMargin = False
        Me.ContextMenuLivel.Size = New System.Drawing.Size(123, 48)
        '
        'УровеньToolStripMenuItem
        '
        Me.УровеньToolStripMenuItem.Name = "УровеньToolStripMenuItem"
        Me.УровеньToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.УровеньToolStripMenuItem.Text = "Уровень +2"
        '
        'УровеньToolStripMenuItem1
        '
        Me.УровеньToolStripMenuItem1.Name = "УровеньToolStripMenuItem1"
        Me.УровеньToolStripMenuItem1.Size = New System.Drawing.Size(122, 22)
        Me.УровеньToolStripMenuItem1.Text = "Уровень -2"
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Lucida Console", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button2.Location = New System.Drawing.Point(252, 101)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(172, 52)
        Me.Button2.TabIndex = 16
        Me.Button2.Text = "Генерировать пароль"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(249, 85)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(87, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Генерация:"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.ActiveLinkColor = System.Drawing.Color.DimGray
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.DisabledLinkColor = System.Drawing.Color.Black
        Me.LinkLabel1.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.LinkLabel1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LinkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.LinkLabel1.LinkColor = System.Drawing.Color.Black
        Me.LinkLabel1.Location = New System.Drawing.Point(353, 64)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(71, 13)
        Me.LinkLabel1.TabIndex = 26
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "> INFO <"
        Me.LinkLabel1.VisitedLinkColor = System.Drawing.Color.Black
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 191)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(71, 13)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Уровень:"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(414, 30)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(20, 23)
        Me.Button3.TabIndex = 28
        Me.Button3.Text = ">"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'PasswordGenerator
        '
        Me.PasswordGenerator.Interval = 10
        '
        'Generator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(442, 229)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TextBox1)
        Me.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MaximizeBox = False
        Me.Name = "Generator"
        Me.Text = "Генератор паролей"
        Me.ContextMenuTextBoxPassword.ResumeLayout(False)
        Me.ContextMenuListBox.ResumeLayout(False)
        Me.ContextMenuLivel.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ContextMenuListBox As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ОчиститьToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents СохранитьВФайлToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents СохранитьВЖурналToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuTextBoxPassword As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents КопироватьToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ОчиститьToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents СохранитьПарольВФайлToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents СохранитьПарольВЖурналToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents ContextMenuLivel As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents УровеньToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents УровеньToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasswordGenerator As System.Windows.Forms.Timer

End Class
