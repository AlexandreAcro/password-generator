﻿Public Class Generator
    Dim library() As String, livels As Integer, simvols() As String, select_livel As Integer, livel_data(3) As String, password As String
    Private Sub ОчиститьToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ОчиститьToolStripMenuItem.Click
        ListBox1.Items.Clear()
    End Sub
    Private Sub ОчиститьToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ОчиститьToolStripMenuItem1.Click
        TextBox1.Text = ""
    End Sub
    Private Sub КопироватьToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles КопироватьToolStripMenuItem.Click
        My.Computer.Clipboard.SetText(TextBox1.Text)
    End Sub
    Private Sub Generator_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Main.Show()
    End Sub
    Private Sub Generator_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        ListBox1.Items.Clear()
        UseWaitCursor = True
        Try
            library = IO.File.ReadAllLines(Main.settings(0))
            Dim end_while As Boolean = False, time_position As Integer = 0, position As Integer = 0
            While end_while <> True
                If position = library.Length Then
                    end_while = True
                    Error 1
                ElseIf library(position) = "[DATA]" Then
                    position += 1
                    end_while = True
                Else
                    position += 1
                End If
            End While
            Array.Resize(simvols, 0)
            end_while = False
            While end_while <> True
                If position = library.Length Then
                    end_while = True
                ElseIf library(position).IndexOf("[") = 0 Then
                    end_while = True
                Else
                    Array.Resize(simvols, simvols.Length + 1)
                    simvols(simvols.Length - 1) = library(position)
                    position += 1
                End If
            End While
            position = 0
            end_while = False
            While end_while <> True
                If position = library.Length Then
                    end_while = True
                    Error 1
                ElseIf library(position) = "[LIVELS]" Then
                    position += 1
                    end_while = True
                Else
                    position += 1
                End If
            End While
            time_position = position
            end_while = False
            While end_while <> True
                If position = library.Length Then
                    Error 1
                ElseIf library(position).IndexOf("[") = 0 Then
                    Error 1
                ElseIf Mid(library(position), 1, 7) = "livels=" Then
                    end_while = True
                Else
                    position += 1
                End If
            End While
            livels = Mid(library(position), 8)
            position = time_position
            end_while = False
            While end_while <> True
                If position = library.Length Then
                    Error 1
                ElseIf library(position).IndexOf("[") = 0 Then
                    Error 1
                ElseIf Mid(library(position), 1, 8) = "simvols=" Then
                    end_while = True
                Else
                    position += 1
                End If
            End While
            Dim simvols_count As Integer = Mid(library(position), 9)
            If simvols_count <> simvols.Length Then Error 1
            Label5.Text = 1 & "/" & livels
            select_livel = 1
            TextBox2.Text = 1
        Catch
            Dim time_dat As String = My.Computer.FileSystem.GetFileInfo(Main.settings(0)).Name
            If time_dat.Length > 29 Then
                MsgBox("Библиотека генерации '" & Mid(time_dat, 1, 24) & "' неисправна! Совет: удалите эту библиотеку.", , "Ошибка")
            Else
                MsgBox("Библиотека генерации '" & Mid(time_dat, 1, time_dat.Length - 5) & "' неисправна! Совет: удалите эту библиотеку.", , "Ошибка")
            End If
            MsgBox("Без рабочей библиотеки генерации функция генерации пароля работать не может!", , "Ошибка")
            Me.Close()
        End Try
        UseWaitCursor = False
    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If select_livel = livels Then
            select_livel = 1
        Else
            select_livel += 1
        End If
        TextBox2.Text = select_livel
        Label5.Text = select_livel & "/" & livels
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If select_livel = 1 Then
            select_livel = livels
        Else
            select_livel -= 1
        End If
        TextBox2.Text = select_livel
        Label5.Text = select_livel & "/" & livels
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            If TextBox2.Text = 0 Or TextBox2.Text > livels Then
                TextBox2.Text = select_livel
            Else
                select_livel = TextBox2.Text
                Label5.Text = select_livel & "/" & livels
            End If
        Catch
            TextBox2.Text = select_livel
        End Try
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
    Private Sub УровеньToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles УровеньToolStripMenuItem.Click
        If select_livel = livels Then
            select_livel = 1
        Else
            If select_livel + 2 > livels Then
                select_livel = 1
            Else
                select_livel += 2
            End If
        End If
            TextBox2.Text = select_livel
            Label5.Text = select_livel & "/" & livels
    End Sub
    Private Sub УровеньToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles УровеньToolStripMenuItem1.Click
        If select_livel = 1 Then
            select_livel = livels
        Else
            If select_livel - 2 = 0 Then
                select_livel = livels
            Else
                select_livel -= 2
            End If
        End If
        TextBox2.Text = select_livel
        Label5.Text = select_livel & "/" & livels
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        UseWaitCursor = True
        Livel_scan()
        password_simvols = 0
        password = ""
        PasswordGenerator.Start()
    End Sub
    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        UseWaitCursor = True
        Livel_scan()
        MsgBox(livel_data(3), , "Информация")
        UseWaitCursor = False
    End Sub
    Private Sub Livel_scan()
        Try
            Dim end_while As Boolean = False, position As Integer = 0
            While end_while <> True
                If position = library.Length Then
                    end_while = True
                    Error 1
                ElseIf library(position) = "[LIVELS]" Then
                    position += 1
                    end_while = True
                Else
                    position += 1
                End If
            End While
            end_while = False
            While end_while <> True
                If position = library.Length Then
                    Error 1
                ElseIf library(position).IndexOf("[") = 0 Then
                    Error 1
                ElseIf library(position).IndexOf("livel:" & select_livel & "=") = 0 Then
                    end_while = True
                Else
                    position += 1
                End If
            End While
            Dim all_no_livel() As String = library(position).Split("=")
            Dim all_no_page() As String = all_no_livel(1).Split(".")
            Dim page_two() As String = all_no_page(0).Split(":")
            livel_data(0) = page_two(1)
            If page_two(0) <> "Start" Then Error 1
            page_two = all_no_page(1).Split(":")
            livel_data(1) = page_two(1)
            If page_two(0) <> "LivelSize" Then Error 1
            page_two = all_no_page(2).Split(":")
            livel_data(2) = page_two(1)
            If page_two(0) <> "Generate" Then Error 1
            livel_data(3) = Mid(library(position), library(position).IndexOf("Info:") + 6)
            Dim tester As Integer = livel_data(0)
            tester = livel_data(1)
            tester = livel_data(2)
        Catch
            Dim time_dat As String = My.Computer.FileSystem.GetFileInfo(Main.settings(0)).Name
            If time_dat.Length > 29 Then
                MsgBox("Библиотека генерации '" & Mid(time_dat, 1, 24) & "' неисправна! Совет: удалите эту библиотеку.", , "Ошибка")
            Else
                MsgBox("Библиотека генерации '" & Mid(time_dat, 1, time_dat.Length - 5) & "' неисправна! Совет: удалите эту библиотеку.", , "Ошибка")
            End If
            MsgBox("Без рабочей библиотеки генерации функция генерации пароля работать не может!", , "Ошибка")
            Me.Close()
        End Try
    End Sub
    Dim password_simvols As Integer = 0
    Private Sub PasswordGenerator_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasswordGenerator.Tick
        Try
            If password_simvols = livel_data(2) Then
                PasswordGenerator.Stop()
                If password.IndexOf(" ") > -1 Then Error 1
                For i As Integer = 1 To Main.Koding(True, password).ToString.Length Step 9
                    If i + 9 <= Main.Koding(True, password).ToString.Length Then
                        Dim time As Integer = Mid(Main.Koding(True, password), i, 9)
                    Else
                        Dim time As Integer = Mid(Main.Koding(True, password), i)
                    End If
                Next
                TextBox1.Text = password
                ListBox1.Items.Add(password)
                password = ""
                ListBox1.SelectedIndex = ListBox1.Items.Count - 1
                UseWaitCursor = False
            Else
                Dim r As New Random
                Dim simvol_number As Integer = r.Next(livel_data(1))
                password &= simvols((livel_data(0) - 1) + simvol_number)
                password_simvols += 1
            End If
        Catch
            PasswordGenerator.Stop()
            Dim time_dat As String = My.Computer.FileSystem.GetFileInfo(Main.settings(0)).Name
            If time_dat.Length > 29 Then
                MsgBox("Библиотека генерации '" & Mid(time_dat, 1, 24) & "' неисправна! Совет: удалите эту библиотеку.", , "Ошибка")
            Else
                MsgBox("Библиотека генерации '" & Mid(time_dat, 1, time_dat.Length - 5) & "' неисправна! Совет: удалите эту библиотеку.", , "Ошибка")
            End If
            MsgBox("Без рабочей библиотеки генерации функция генерации пароля работать не может!", , "Ошибка")
            Me.Close()
        End Try
    End Sub
    Private Sub СохранитьВФайлToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles СохранитьВФайлToolStripMenuItem.Click
        Try
            If My.Computer.FileSystem.FileExists(Main.settings(5)) Then
                Dim time_dat() As String = IO.File.ReadAllLines(Main.settings(5))
                Array.Resize(time_dat, time_dat.Length + 1)
                time_dat(time_dat.Length - 1) = "Пароль: " & ListBox1.SelectedItem
                IO.File.WriteAllLines(Main.settings(5), time_dat)
            Else
                My.Computer.FileSystem.WriteAllText(Main.settings(5), "Пароль: " & ListBox1.SelectedItem, False)
            End If
            MsgBox("Сохранено завершено успешно!", , "Сообщение")
        Catch
            MsgBox("Сохранение не выполнено: нет доступа к файлу!", , "Ошибка")
        End Try
    End Sub
    Private Sub ContextMenuListBox_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ContextMenuListBox.Opening
        If ListBox1.SelectedIndex > -1 Then
            ContextMenuListBox.Items(1).Enabled = True
            ContextMenuListBox.Items(2).Enabled = True
        Else
            ContextMenuListBox.Items(1).Enabled = False
            ContextMenuListBox.Items(2).Enabled = False
        End If
    End Sub
    Private Sub ContextMenuTextBoxPassword_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ContextMenuTextBoxPassword.Opening
        If TextBox1.Text = "" Then
            ContextMenuTextBoxPassword.Items(1).Enabled = False
            ContextMenuTextBoxPassword.Items(2).Enabled = False
            ContextMenuTextBoxPassword.Items(3).Enabled = False
        Else
            ContextMenuTextBoxPassword.Items(1).Enabled = True
            ContextMenuTextBoxPassword.Items(2).Enabled = True
            ContextMenuTextBoxPassword.Items(3).Enabled = True
        End If
    End Sub
    Private Sub СохранитьПарольВФайлToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles СохранитьПарольВФайлToolStripMenuItem.Click
        Try
            If My.Computer.FileSystem.FileExists(Main.settings(5)) Then
                Dim time_dat() As String = IO.File.ReadAllLines(Main.settings(5))
                Array.Resize(time_dat, time_dat.Length + 1)
                time_dat(time_dat.Length - 1) = "Пароль: " & TextBox1.Text
                IO.File.WriteAllLines(Main.settings(5), time_dat)
            Else
                My.Computer.FileSystem.WriteAllText(Main.settings(5), "Пароль: " & TextBox1.Text, False)
            End If
            MsgBox("Сохранено завершено успешно!", , "Сообщение")
        Catch
            MsgBox("Сохранение не выполнено: нет доступа к файлу!", , "Ошибка")
        End Try
    End Sub
    Private Sub СохранитьПарольВЖурналToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles СохранитьПарольВЖурналToolStripMenuItem.Click
        AddPassword.Button1.Tag = "GeneratedPassword"
        AddPassword.ShowDialog()
    End Sub
    Private Sub СохранитьВЖурналToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles СохранитьВЖурналToolStripMenuItem.Click
        AddPassword.Button1.Tag = "LastGeneratedPassword"
        AddPassword.ShowDialog()
    End Sub
End Class
