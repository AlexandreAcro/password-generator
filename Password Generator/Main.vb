﻿Imports System.ComponentModel

Public Class Main
    Public Shared settings() As String, journal() As String, way As String = Application.StartupPath, libs() As String, info_data_bases(5) As String
    Dim pg_db As String = Application.StartupPath & "\data.dat", ps_data As New List(Of Data_segment), my_number As String = "0"
    Dim temp_way As String = Environ("Temp") & "\pg_606579847089587989570.tmp"
    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        my_number = My.Settings.MyKode
        If my_number = "" Then my_number = "0"
        Try
            My.Computer.FileSystem.WriteAllText(way & "\test.txt", "", False)
            My.Computer.FileSystem.DeleteFile(way & "\test.txt")
        Catch
            MsgBox("Генератор паролей был установлен некорректно!" & Chr(10) & "Возможная причина ошибки: Генератору паролей для работы на данном компьютере необходимы права администратора (актуальна для Windows 7 и выше), проблемы совместимости с вашей версией или пакетом обновления Windows (вероятна для Windows 8 и выше)." & Chr(10) & "Совет: Попробуйте воспользоваться средствами исправления неполадок совместимости или предоставьте Генератору паролей права администратора. Если вышеперечисленное не помогло, попробуйте переустановить программу.", , "Ошибка")
            fast_close = True
            Me.Close()
        End Try
        Dim proc() As Process = Process.GetProcesses, my_way As String = Application.ExecutablePath
        If My.Computer.FileSystem.FileExists(temp_way) = False Then
            My.Computer.FileSystem.WriteAllText(temp_way, my_way, False)
        Else
            Dim td_j_dat As String = My.Computer.FileSystem.ReadAllText(temp_way), is_duble As Short = 0
            For Each i As Process In proc
                Try
                    If i.MainModule.FileName.ToLower = my_way.ToLower Then
                        is_duble += 1
                    ElseIf i.MainModule.FileName.ToLower = td_j_dat.ToLower Then
                        is_duble = 2
                    End If
                    If is_duble = 2 Then
                        Exit For
                    End If
                Catch
                End Try
            Next
            If is_duble = 2 Then
                MsgBox("Генератор паролей уже запущен!", , "Ошибка")
                fast_close = True
                Me.Close()
            Else
                My.Computer.FileSystem.WriteAllText(temp_way, my_way, False)
            End If
        End If
        If My.Computer.FileSystem.FileExists(pg_db) <> True Then
            My.Computer.FileSystem.WriteAllText(pg_db, "", False)
            Array.Resize(settings, 6)
            settings(0) = way & "/Generate Libraries/GPSL.glib"
            settings(1) = ""
            settings(2) = True
            settings(3) = True
            settings(4) = "Prewiev"
            settings(5) = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\Ваш пароль.txt"
            WriteFile("Settings", settings)
            Array.Resize(journal, 0)
            WriteFile("Journal", journal)
            PasswordGen_Write()
        Else
            Dim pg_db_data() As String = IO.File.ReadAllLines(pg_db), pg_db_code As String = ""
            pg_db_code = Koding(False, pg_db_data(pg_db_data.Length - 1))
            If pg_db_code = my_number Then
                Array.Resize(pg_db_data, pg_db_data.Length - 1)
                pg_db_data = CryptText(my_number, pg_db_data, False)
                Try
                    For i As Integer = 0 To pg_db_data.Length - 1
                        If "<" & pg_db_data(i).TrimStart("<") = pg_db_data(i) Then
                            ps_data.Add(New Data_segment)
                            ps_data.Item(ps_data.Count - 1).name = pg_db_data(i).TrimStart("<")
                            Dim simvol As String = "", i1 As Integer = i
                            While simvol <> ">"
                                simvol = pg_db_data(i1)
                                i1 += 1
                            End While
                            For i2 As Integer = i + 1 To i1 - 2
                                Array.Resize(ps_data.Item(ps_data.Count - 1).data, ps_data.Item(ps_data.Count - 1).data.Length + 1)
                                ps_data.Item(ps_data.Count - 1).data(ps_data.Item(ps_data.Count - 1).data.Length - 1) = pg_db_data(i2)
                            Next
                        End If
                    Next
                Catch
                    MsgBox("База данных программы повреждена и будет пересоздана.", MsgBoxStyle.OkOnly, "Сообщение")
                    My.Computer.FileSystem.WriteAllText(pg_db, "", False)
                    Array.Resize(settings, 6)
                    settings(0) = way & "/Generate Libraries/GPSL.glib"
                    settings(1) = ""
                    settings(2) = True
                    settings(3) = True
                    settings(4) = "Prewiev"
                    settings(5) = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\Ваш пароль.txt"
                    WriteFile("Settings", settings)
                    Array.Resize(journal, 0)
                    WriteFile("Journal", journal)
                    PasswordGen_Write()
                End Try
                If FileExits("Settings") = False Or FileExits("Journal") = False Then
                    MsgBox("База данных программы повреждена и будет пересоздана.", MsgBoxStyle.OkOnly, "Сообщение")
                    My.Computer.FileSystem.WriteAllText(pg_db, "", False)
                    Array.Resize(settings, 6)
                    settings(0) = way & "/Generate Libraries/GPSL.glib"
                    settings(1) = ""
                    settings(2) = True
                    settings(3) = True
                    settings(4) = "Prewiev"
                    settings(5) = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\Ваш пароль.txt"
                    WriteFile("Settings", settings)
                    Array.Resize(journal, 0)
                    WriteFile("Journal", journal)
                    PasswordGen_Write()
                Else
                    If FileExits("Settings") Then
                        settings = ReadFile("Settings")
                    End If
                    If FileExits("Journal") Then
                        journal = ReadFile("Journal")
                    End If
                    Try
                        If settings(2) <> True And settings(2) <> False Then Error 1
                        If settings(3) <> True And settings(3) <> False Then Error 1
                        If settings(4) <> "Prewiev" And settings(4) <> "Redaction" Then Error 1
                        For i As Integer = 1 To Koding(True, settings(1)).ToString.Length Step 9
                            If i + 9 <= Koding(True, settings(1)).ToString.Length Then
                                Dim time As Integer = Mid(Koding(True, settings(1)), i, 9)
                            Else
                                Dim time As Integer = Mid(Koding(True, settings(1)), i)
                            End If
                        Next
                    Catch
                        MsgBox("База данных программы повреждена и будет пересоздана.", MsgBoxStyle.OkOnly, "Сообщение")
                        My.Computer.FileSystem.WriteAllText(pg_db, "", False)
                        Array.Resize(settings, 6)
                        settings(0) = way & "/Generate Libraries/GPSL.glib"
                        settings(1) = ""
                        settings(2) = True
                        settings(3) = True
                        settings(4) = "Prewiev"
                        settings(5) = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\Ваш пароль.txt"
                        WriteFile("Settings", settings)
                        Array.Resize(journal, 0)
                        WriteFile("Journal", journal)
                        PasswordGen_Write()
                    End Try
                End If
            Else
                MsgBox("База данных программы повреждена и будет пересоздана.", MsgBoxStyle.OkOnly, "Сообщение")
                My.Computer.FileSystem.WriteAllText(pg_db, "", False)
                Array.Resize(settings, 6)
                settings(0) = way & "/Generate Libraries/GPSL.glib"
                settings(1) = ""
                settings(2) = True
                settings(3) = True
                settings(4) = "Prewiev"
                settings(5) = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\Ваш пароль.txt"
                WriteFile("Settings", settings)
                Array.Resize(journal, 0)
                WriteFile("Journal", journal)
                PasswordGen_Write()
            End If
        End If
        If my_number = "0" Then
            Dim r As New Random
            nums = r.Next(2, 17)
            Button1.Enabled = False
            Button2.Enabled = False
            Button3.Enabled = False
            Button4.Enabled = False
            ContextMenu1.Enabled = False
            MyCodeGenerator.Start()
        End If
    End Sub
    Private Sub Main_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        If My.Computer.FileSystem.DirectoryExists(way & "/Generate Libraries") = False Then
            My.Computer.FileSystem.CreateDirectory(way & "/Generate Libraries")
            My.Computer.FileSystem.WriteAllBytes(way & "/Generate Libraries/GPSL.glib", My.Resources.GPSL, False)
            Array.Resize(libs, 1)
            libs(0) = way & "/Generate Libraries/GPSL.glib"
        Else
            If My.Computer.FileSystem.FileExists(way & "/Generate Libraries/GPSL.glib") = False Then
                My.Computer.FileSystem.WriteAllBytes(way & "/Generate Libraries/GPSL.glib", My.Resources.GPSL, False)
            End If
            libs = IO.Directory.GetFiles(way & "/Generate Libraries", "*.glib")
        End If
        If My.Computer.FileSystem.FileExists(settings(0)) = False Then
            settings(0) = way & "/Generate Libraries/GPSL.glib"
        End If
    End Sub
#Region "Text inspection"
    Public Function Koding(ByVal create As Boolean, ByVal text As String)
        Dim otvet As String = ""
        If create = True Then
            For i As Integer = 0 To text.Length - 1
                Dim simvol As String = Mid(text, i + 1, 1)
                If simvol = "q" Then
                    otvet &= "8619"
                ElseIf simvol = "w" Then
                    otvet &= "7619"
                ElseIf simvol = "e" Then
                    otvet &= "9461"
                ElseIf simvol = "r" Then
                    otvet &= "7854"
                ElseIf simvol = "t" Then
                    otvet &= "9767"
                ElseIf simvol = "y" Then
                    otvet &= "8761"
                ElseIf simvol = "u" Then
                    otvet &= "7606"
                ElseIf simvol = "i" Then
                    otvet &= "6094"
                ElseIf simvol = "o" Then
                    otvet &= "9721"
                ElseIf simvol = "p" Then
                    otvet &= "0815"
                ElseIf simvol = "a" Then
                    otvet &= "9791"
                ElseIf simvol = "s" Then
                    otvet &= "0601"
                ElseIf simvol = "d" Then
                    otvet &= "9098"
                ElseIf simvol = "f" Then
                    otvet &= "9731"
                ElseIf simvol = "g" Then
                    otvet &= "9723"
                ElseIf simvol = "h" Then
                    otvet &= "0341"
                ElseIf simvol = "j" Then
                    otvet &= "8790"
                ElseIf simvol = "k" Then
                    otvet &= "9760"
                ElseIf simvol = "l" Then
                    otvet &= "9745"
                ElseIf simvol = "z" Then
                    otvet &= "9720"
                ElseIf simvol = "x" Then
                    otvet &= "9732"
                ElseIf simvol = "c" Then
                    otvet &= "4157"
                ElseIf simvol = "v" Then
                    otvet &= "3720"
                ElseIf simvol = "b" Then
                    otvet &= "6431"
                ElseIf simvol = "n" Then
                    otvet &= "7186"
                ElseIf simvol = "m" Then
                    otvet &= "9676"
                ElseIf simvol = "Q" Then
                    otvet &= "0797"
                ElseIf simvol = "W" Then
                    otvet &= "6408"
                ElseIf simvol = "E" Then
                    otvet &= "0346"
                ElseIf simvol = "R" Then
                    otvet &= "0973"
                ElseIf simvol = "T" Then
                    otvet &= "0975"
                ElseIf simvol = "Y" Then
                    otvet &= "5011"
                ElseIf simvol = "U" Then
                    otvet &= "1100"
                ElseIf simvol = "I" Then
                    otvet &= "2201"
                ElseIf simvol = "O" Then
                    otvet &= "6108"
                ElseIf simvol = "P" Then
                    otvet &= "0108"
                ElseIf simvol = "A" Then
                    otvet &= "0671"
                ElseIf simvol = "S" Then
                    otvet &= "3471"
                ElseIf simvol = "D" Then
                    otvet &= "0467"
                ElseIf simvol = "F" Then
                    otvet &= "0124"
                ElseIf simvol = "G" Then
                    otvet &= "0132"
                ElseIf simvol = "H" Then
                    otvet &= "0143"
                ElseIf simvol = "J" Then
                    otvet &= "0132"
                ElseIf simvol = "K" Then
                    otvet &= "0608"
                ElseIf simvol = "L" Then
                    otvet &= "0802"
                ElseIf simvol = "Z" Then
                    otvet &= "6701"
                ElseIf simvol = "X" Then
                    otvet &= "0121"
                ElseIf simvol = "C" Then
                    otvet &= "1221"
                ElseIf simvol = "V" Then
                    otvet &= "4004"
                ElseIf simvol = "B" Then
                    otvet &= "2002"
                ElseIf simvol = "N" Then
                    otvet &= "1230"
                ElseIf simvol = "M" Then
                    otvet &= "1028"
                ElseIf simvol = "1" Then
                    otvet &= "9751"
                ElseIf simvol = "2" Then
                    otvet &= "2464"
                ElseIf simvol = "3" Then
                    otvet &= "3464"
                ElseIf simvol = "4" Then
                    otvet &= "9741"
                ElseIf simvol = "5" Then
                    otvet &= "3104"
                ElseIf simvol = "6" Then
                    otvet &= "3451"
                ElseIf simvol = "7" Then
                    otvet &= "6410"
                ElseIf simvol = "8" Then
                    otvet &= "9724"
                ElseIf simvol = "9" Then
                    otvet &= "6421"
                ElseIf simvol = "0" Then
                    otvet &= "2120"
                ElseIf simvol = "!" Then
                    otvet &= "0780"
                ElseIf simvol = "@" Then
                    otvet &= "9762"
                ElseIf simvol = "#" Then
                    otvet &= "3421"
                ElseIf simvol = "$" Then
                    otvet &= "6721"
                ElseIf simvol = "%" Then
                    otvet &= "3197"
                ElseIf simvol = "^" Then
                    otvet &= "6497"
                ElseIf simvol = "&" Then
                    otvet &= "6439"
                ElseIf simvol = "*" Then
                    otvet &= "0010"
                ElseIf simvol = "(" Then
                    otvet &= "0020"
                ElseIf simvol = ")" Then
                    otvet &= "0004"
                ElseIf simvol = "-" Then
                    otvet &= "3067"
                ElseIf simvol = "=" Then
                    otvet &= "0600"
                ElseIf simvol = "+" Then
                    otvet &= "1000"
                ElseIf simvol = "?" Then
                    otvet &= "9700"
                ElseIf simvol = "~" Then
                    otvet &= "0347"
                ElseIf simvol = "№" Then
                    otvet &= "3401"
                ElseIf simvol = "_" Then
                    otvet &= "9670"
                ElseIf simvol = "." Then
                    otvet &= "9701"
                ElseIf simvol = "," Then
                    otvet &= "9702"
                ElseIf simvol = ":" Then
                    otvet &= "1079"
                ElseIf simvol = ";" Then
                    otvet &= "1690"
                Else
                    otvet &= simvol
                End If
            Next
        Else
            For i As Integer = 0 To text.Length - 1 Step 4
                Dim simvol As String = Mid(text, i + 1, 4)
                If simvol = "8619" Then
                    otvet &= "q"
                ElseIf simvol = "7619" Then
                    otvet &= "w"
                ElseIf simvol = "9461" Then
                    otvet &= "e"
                ElseIf simvol = "7854" Then
                    otvet &= "r"
                ElseIf simvol = "9767" Then
                    otvet &= "t"
                ElseIf simvol = "8761" Then
                    otvet &= "y"
                ElseIf simvol = "7606" Then
                    otvet &= "u"
                ElseIf simvol = "6094" Then
                    otvet &= "i"
                ElseIf simvol = "9721" Then
                    otvet &= "o"
                ElseIf simvol = "0815" Then
                    otvet &= "p"
                ElseIf simvol = "9791" Then
                    otvet &= "a"
                ElseIf simvol = "0601" Then
                    otvet &= "s"
                ElseIf simvol = "9098" Then
                    otvet &= "d"
                ElseIf simvol = "9731" Then
                    otvet &= "f"
                ElseIf simvol = "9723" Then
                    otvet &= "g"
                ElseIf simvol = "0341" Then
                    otvet &= "h"
                ElseIf simvol = "8790" Then
                    otvet &= "j"
                ElseIf simvol = "9760" Then
                    otvet &= "k"
                ElseIf simvol = "9745" Then
                    otvet &= "l"
                ElseIf simvol = "9720" Then
                    otvet &= "z"
                ElseIf simvol = "9732" Then
                    otvet &= "x"
                ElseIf simvol = "4157" Then
                    otvet &= "c"
                ElseIf simvol = "3720" Then
                    otvet &= "v"
                ElseIf simvol = "6431" Then
                    otvet &= "b"
                ElseIf simvol = "7186" Then
                    otvet &= "n"
                ElseIf simvol = "9676" Then
                    otvet &= "m"
                ElseIf simvol = "0797" Then
                    otvet &= "Q"
                ElseIf simvol = "6408" Then
                    otvet &= "W"
                ElseIf simvol = "0346" Then
                    otvet &= "E"
                ElseIf simvol = "0973" Then
                    otvet &= "R"
                ElseIf simvol = "0975" Then
                    otvet &= "T"
                ElseIf simvol = "5011" Then
                    otvet &= "Y"
                ElseIf simvol = "1100" Then
                    otvet &= "U"
                ElseIf simvol = "2201" Then
                    otvet &= "I"
                ElseIf simvol = "6108" Then
                    otvet &= "O"
                ElseIf simvol = "0108" Then
                    otvet &= "P"
                ElseIf simvol = "0671" Then
                    otvet &= "A"
                ElseIf simvol = "3471" Then
                    otvet &= "S"
                ElseIf simvol = "0467" Then
                    otvet &= "D"
                ElseIf simvol = "0124" Then
                    otvet &= "F"
                ElseIf simvol = "0132" Then
                    otvet &= "G"
                ElseIf simvol = "0143" Then
                    otvet &= "H"
                ElseIf simvol = "0132" Then
                    otvet &= "J"
                ElseIf simvol = "0608" Then
                    otvet &= "K"
                ElseIf simvol = "0802" Then
                    otvet &= "L"
                ElseIf simvol = "6701" Then
                    otvet &= "Z"
                ElseIf simvol = "0121" Then
                    otvet &= "X"
                ElseIf simvol = "1221" Then
                    otvet &= "C"
                ElseIf simvol = "4004" Then
                    otvet &= "V"
                ElseIf simvol = "2002" Then
                    otvet &= "B"
                ElseIf simvol = "1230" Then
                    otvet &= "N"
                ElseIf simvol = "1028" Then
                    otvet &= "M"
                ElseIf simvol = "9751" Then
                    otvet &= "1"
                ElseIf simvol = "2464" Then
                    otvet &= "2"
                ElseIf simvol = "3464" Then
                    otvet &= "3"
                ElseIf simvol = "9741" Then
                    otvet &= "4"
                ElseIf simvol = "3104" Then
                    otvet &= "5"
                ElseIf simvol = "3451" Then
                    otvet &= "6"
                ElseIf simvol = "6410" Then
                    otvet &= "7"
                ElseIf simvol = "9724" Then
                    otvet &= "8"
                ElseIf simvol = "6421" Then
                    otvet &= "9"
                ElseIf simvol = "2120" Then
                    otvet &= "0"
                ElseIf simvol = "0780" Then
                    otvet &= "!"
                ElseIf simvol = "9762" Then
                    otvet &= "@"
                ElseIf simvol = "3421" Then
                    otvet &= "#"
                ElseIf simvol = "6721" Then
                    otvet &= "$"
                ElseIf simvol = "3197" Then
                    otvet &= "%"
                ElseIf simvol = "6497" Then
                    otvet &= "^"
                ElseIf simvol = "6439" Then
                    otvet &= "&"
                ElseIf simvol = "0010" Then
                    otvet &= "*"
                ElseIf simvol = "0020" Then
                    otvet &= "("
                ElseIf simvol = "0004" Then
                    otvet &= ")"
                ElseIf simvol = "3067" Then
                    otvet &= "-"
                ElseIf simvol = "0600" Then
                    otvet &= "="
                ElseIf simvol = "1000" Then
                    otvet &= "+"
                ElseIf simvol = "9700" Then
                    otvet &= "?"
                ElseIf simvol = "0347" Then
                    otvet &= "~"
                ElseIf simvol = "3401" Then
                    otvet &= "№"
                ElseIf simvol = "9670" Then
                    otvet &= "_"
                ElseIf simvol = "9701" Then
                    otvet &= "."
                ElseIf simvol = "9702" Then
                    otvet &= ","
                ElseIf simvol = "1079" Then
                    otvet &= ":"
                ElseIf simvol = "1690" Then
                    otvet &= ";"
                Else
                    otvet &= Mid(text, i + 1, 1)
                    i -= 3
                End If
            Next
        End If
        Return otvet
    End Function
#End Region
#Region "Text Cription"
    Private Function CryptText(ByVal Kod As String, ByVal InputText() As String, ByVal IsCrypt As Boolean) As String()
        Dim arrayline As String = ""
        For i As Integer = 0 To InputText.Length - 1
            If i + 1 = InputText.Length Then
                arrayline &= InputText(i)
            Else
                arrayline &= InputText(i) & Chr(13) & Chr(10)
            End If
        Next
        Dim ByteIn As Byte() = System.Text.Encoding.Unicode.GetBytes(arrayline)
        Dim ByteOut(ByteIn.Length - 1) As Byte
        Dim Tmp As Integer = 0
        For I As Integer = 0 To ByteIn.Length - 1
            If Tmp >= Kod.Length Then Tmp = 0
            ByteOut(I) = ByteCrypt(Kod(Tmp), ByteIn(I), IsCrypt)
            Tmp += 1
        Next
        Dim OutputTextArray As String = System.Text.Encoding.Unicode.GetChars(ByteOut), OutputText() As String = OutputTextArray.Split(Chr(13) & Chr(10))
        For i As Integer = 0 To OutputText.Length - 1
            OutputText(i) = OutputText(i).TrimStart(Chr(10))
        Next
        Return OutputText
    End Function
    Private Function ByteCrypt(ByVal S As Char, ByVal Inp As Byte, ByVal Crypt As Boolean) As Byte
        If Crypt = True Then
            If Inp + Asc(S) > 255 Then
                Return Inp + Asc(S) - 256
            Else
                Return Inp + Asc(S)
            End If
        Else
            If Inp - Asc(S) < 0 Then
                Return 256 + Inp - Asc(S)
            Else
                Return Inp - Asc(S)
            End If
        End If
    End Function
#End Region
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Settings_Program.ShowDialog()
    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Prewiev.Show()
        Me.Hide()
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Generator.Show()
        Me.Hide()
    End Sub
    Private Sub ГенерацияПаролейToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ГенерацияПаролейToolStripMenuItem.Click
        Generator.Show()
        Me.Hide()
    End Sub
    Dim nums As Short, my_num As String = ""
    Private Sub MyCodeGenerator_Tick(sender As Object, e As EventArgs) Handles MyCodeGenerator.Tick
        If nums = 0 Then
            MyCodeGenerator.Stop()
            my_number = my_num
            My.Settings.MyKode = my_number
            My.Settings.Save()
            PasswordGen_Write()
            Button1.Enabled = True
            Button2.Enabled = True
            Button3.Enabled = True
            Button4.Enabled = True
            ContextMenu1.Enabled = True
        Else
            Dim r As New Random
            my_num &= r.Next(10)
            nums -= 1
        End If
    End Sub
    Private Sub ЖурналToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ЖурналToolStripMenuItem.Click
        Prewiev.Show()
        Me.Hide()
    End Sub
    Private Sub НастройкиToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles НастройкиToolStripMenuItem.Click
        Settings_Program.ShowDialog()
    End Sub
    Private Sub ВыходToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ВыходToolStripMenuItem.Click
        Me.Close()
    End Sub
    Private Sub ОПрограммеToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ОПрограммеToolStripMenuItem.Click
        AboutProgramm.Show()
    End Sub
#Region "File System"
    Public Sub Settings_Write()
        WriteFile("Settings", settings)
        PasswordGen_Write()
    End Sub
    Public Sub Journal_Write()
        WriteFile("Journal", journal)
        PasswordGen_Write()
    End Sub
    Public Function FileExits(file As String)
        Dim exits As Boolean = False
        For i As Integer = 0 To ps_data.Count - 1
            If ps_data(i).name = file Then
                exits = True
                Exit For
            End If
        Next
        Return exits
    End Function
    Public Sub WriteFile(file As String, data() As String)
        If FileExits(file) Then
            For i As Integer = 0 To ps_data.Count - 1
                If ps_data(i).name = file Then
                    Array.Resize(ps_data(i).data, data.Length)
                    For i1 As Integer = 0 To data.Length - 1
                        ps_data(i).data(i1) = data(i1)
                    Next
                    Exit For
                End If
            Next
        Else
            ps_data.Add(New Data_segment)
            ps_data.Item(ps_data.Count - 1).name = file
            Array.Resize(ps_data(ps_data.Count - 1).data, data.Length)
            For i As Integer = 0 To data.Length - 1
                ps_data(ps_data.Count - 1).data(i) = data(i)
            Next
        End If
    End Sub
    Public Function ReadFile(file As String)
        Dim data(-1) As String
        If FileExits(file) Then
            For i As Integer = 0 To ps_data.Count - 1
                If ps_data(i).name = file Then
                    Array.Resize(data, ps_data(i).data.Length)
                    For i1 As Integer = 0 To data.Length - 1
                        data(i1) = ps_data(i).data(i1)
                    Next
                End If
            Next
        End If
        Return data
    End Function
    Private Sub PasswordGen_Write()
        Dim filedata(-1) As String
        For i As Integer = 0 To ps_data.Count - 1
            Array.Resize(filedata, filedata.Length + 1)
            filedata(filedata.Length - 1) = "<" & ps_data.Item(i).name
            For i1 As Integer = 0 To ps_data.Item(i).data.Length - 1
                Array.Resize(filedata, filedata.Length + 1)
                filedata(filedata.Length - 1) = ps_data.Item(i).data(i1)
            Next
            Array.Resize(filedata, filedata.Length + 1)
            filedata(filedata.Length - 1) = ">"
        Next
        filedata = CryptText(my_number, filedata, True)
        Array.Resize(filedata, filedata.Length + 1)
        filedata(filedata.Length - 1) = Koding(True, my_number)
        IO.File.WriteAllLines(pg_db, filedata)
    End Sub
    Dim fast_close As Boolean = False
    Private Sub Main_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If fast_close = False Then
            My.Computer.FileSystem.DeleteFile(temp_way)
        End If
    End Sub
    Private Class Data_segment
        Public name As String, data(-1) As String
    End Class
#End Region
End Class

