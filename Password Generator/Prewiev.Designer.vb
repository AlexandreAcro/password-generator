﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Prewiev
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Prewiev))
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ContextMenuListBox = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.УдалитьToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ИзменитьToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.КопироватьToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ИмяToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ПарольToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ИмяПарольToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.СохранитьToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ИмяToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ПарольToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ВсеToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.БыстроеСохранениеToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ИмяToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ПарольToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ИмяПарольToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ПередатьпринятьЖурналToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.SaveFile = New System.Windows.Forms.SaveFileDialog()
        Me.ContextMenuListBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'ListBox1
        '
        Me.ListBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListBox1.ContextMenuStrip = Me.ContextMenuListBox
        Me.ListBox1.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.HorizontalScrollbar = True
        Me.ListBox1.ItemHeight = 21
        Me.ListBox1.Location = New System.Drawing.Point(12, 12)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(440, 235)
        Me.ListBox1.TabIndex = 0
        '
        'ContextMenuListBox
        '
        Me.ContextMenuListBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.ContextMenuListBox.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.УдалитьToolStripMenuItem, Me.ToolStripSeparator3, Me.ИзменитьToolStripMenuItem, Me.ToolStripSeparator2, Me.КопироватьToolStripMenuItem, Me.СохранитьToolStripMenuItem, Me.БыстроеСохранениеToolStripMenuItem, Me.ToolStripSeparator1, Me.ПередатьпринятьЖурналToolStripMenuItem})
        Me.ContextMenuListBox.Name = "ContextMenuListBox"
        Me.ContextMenuListBox.ShowImageMargin = False
        Me.ContextMenuListBox.Size = New System.Drawing.Size(210, 154)
        '
        'УдалитьToolStripMenuItem
        '
        Me.УдалитьToolStripMenuItem.Name = "УдалитьToolStripMenuItem"
        Me.УдалитьToolStripMenuItem.Size = New System.Drawing.Size(209, 22)
        Me.УдалитьToolStripMenuItem.Text = "Удалить"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(206, 6)
        '
        'ИзменитьToolStripMenuItem
        '
        Me.ИзменитьToolStripMenuItem.Name = "ИзменитьToolStripMenuItem"
        Me.ИзменитьToolStripMenuItem.Size = New System.Drawing.Size(209, 22)
        Me.ИзменитьToolStripMenuItem.Text = "Изменить"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(206, 6)
        '
        'КопироватьToolStripMenuItem
        '
        Me.КопироватьToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ИмяToolStripMenuItem, Me.ПарольToolStripMenuItem, Me.ИмяПарольToolStripMenuItem})
        Me.КопироватьToolStripMenuItem.Name = "КопироватьToolStripMenuItem"
        Me.КопироватьToolStripMenuItem.Size = New System.Drawing.Size(209, 22)
        Me.КопироватьToolStripMenuItem.Text = "Копировать"
        '
        'ИмяToolStripMenuItem
        '
        Me.ИмяToolStripMenuItem.Name = "ИмяToolStripMenuItem"
        Me.ИмяToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.ИмяToolStripMenuItem.Text = "Имя"
        '
        'ПарольToolStripMenuItem
        '
        Me.ПарольToolStripMenuItem.Name = "ПарольToolStripMenuItem"
        Me.ПарольToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.ПарольToolStripMenuItem.Text = "Пароль"
        '
        'ИмяПарольToolStripMenuItem
        '
        Me.ИмяПарольToolStripMenuItem.Name = "ИмяПарольToolStripMenuItem"
        Me.ИмяПарольToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.ИмяПарольToolStripMenuItem.Text = "Имя + Пароль"
        '
        'СохранитьToolStripMenuItem
        '
        Me.СохранитьToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ИмяToolStripMenuItem1, Me.ПарольToolStripMenuItem1, Me.ВсеToolStripMenuItem})
        Me.СохранитьToolStripMenuItem.Name = "СохранитьToolStripMenuItem"
        Me.СохранитьToolStripMenuItem.Size = New System.Drawing.Size(209, 22)
        Me.СохранитьToolStripMenuItem.Text = "Сохранить"
        '
        'ИмяToolStripMenuItem1
        '
        Me.ИмяToolStripMenuItem1.Name = "ИмяToolStripMenuItem1"
        Me.ИмяToolStripMenuItem1.Size = New System.Drawing.Size(165, 22)
        Me.ИмяToolStripMenuItem1.Text = "Имя"
        '
        'ПарольToolStripMenuItem1
        '
        Me.ПарольToolStripMenuItem1.Name = "ПарольToolStripMenuItem1"
        Me.ПарольToolStripMenuItem1.Size = New System.Drawing.Size(165, 22)
        Me.ПарольToolStripMenuItem1.Text = "Пароль"
        '
        'ВсеToolStripMenuItem
        '
        Me.ВсеToolStripMenuItem.Name = "ВсеToolStripMenuItem"
        Me.ВсеToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.ВсеToolStripMenuItem.Text = "Имя + Пароль"
        '
        'БыстроеСохранениеToolStripMenuItem
        '
        Me.БыстроеСохранениеToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ИмяToolStripMenuItem2, Me.ПарольToolStripMenuItem2, Me.ИмяПарольToolStripMenuItem1})
        Me.БыстроеСохранениеToolStripMenuItem.Name = "БыстроеСохранениеToolStripMenuItem"
        Me.БыстроеСохранениеToolStripMenuItem.Size = New System.Drawing.Size(209, 22)
        Me.БыстроеСохранениеToolStripMenuItem.Text = "Cохранить в другой файл"
        '
        'ИмяToolStripMenuItem2
        '
        Me.ИмяToolStripMenuItem2.Name = "ИмяToolStripMenuItem2"
        Me.ИмяToolStripMenuItem2.Size = New System.Drawing.Size(165, 22)
        Me.ИмяToolStripMenuItem2.Text = "Имя"
        '
        'ПарольToolStripMenuItem2
        '
        Me.ПарольToolStripMenuItem2.Name = "ПарольToolStripMenuItem2"
        Me.ПарольToolStripMenuItem2.Size = New System.Drawing.Size(165, 22)
        Me.ПарольToolStripMenuItem2.Text = "Пароль"
        '
        'ИмяПарольToolStripMenuItem1
        '
        Me.ИмяПарольToolStripMenuItem1.Name = "ИмяПарольToolStripMenuItem1"
        Me.ИмяПарольToolStripMenuItem1.Size = New System.Drawing.Size(165, 22)
        Me.ИмяПарольToolStripMenuItem1.Text = "Имя + Пароль"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(206, 6)
        '
        'ПередатьпринятьЖурналToolStripMenuItem
        '
        Me.ПередатьпринятьЖурналToolStripMenuItem.Name = "ПередатьпринятьЖурналToolStripMenuItem"
        Me.ПередатьпринятьЖурналToolStripMenuItem.Size = New System.Drawing.Size(209, 22)
        Me.ПередатьпринятьЖурналToolStripMenuItem.Text = "Передать/принять журнал"
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(346, 267)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(106, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Закрыть"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button2.Location = New System.Drawing.Point(97, 267)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Удалить"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button3.Location = New System.Drawing.Point(12, 267)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(79, 23)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Добавить"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.Location = New System.Drawing.Point(12, 128)
        Me.TextBox1.MaxLength = 1000
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextBox1.Size = New System.Drawing.Size(440, 20)
        Me.TextBox1.TabIndex = 5
        Me.TextBox1.Visible = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Location = New System.Drawing.Point(12, 112)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(440, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Введите пароль для доступа к журналу:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label1.Visible = False
        '
        'Button4
        '
        Me.Button4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button4.Location = New System.Drawing.Point(178, 267)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 7
        Me.Button4.Text = "Поиск"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(324, 154)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(128, 23)
        Me.Button5.TabIndex = 8
        Me.Button5.Text = "Принять пароль"
        Me.Button5.UseVisualStyleBackColor = True
        Me.Button5.Visible = False
        '
        'SaveFile
        '
        Me.SaveFile.Filter = "Текстовые файлы|*.txt"
        Me.SaveFile.Title = "Быстрое сохранение"
        '
        'Prewiev
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(464, 302)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListBox1)
        Me.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(400, 340)
        Me.Name = "Prewiev"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Журнал"
        Me.ContextMenuListBox.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents ContextMenuListBox As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents УдалитьToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents КопироватьToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ИмяToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ПарольToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents СохранитьToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ВсеToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ИмяToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ПарольToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ИмяПарольToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents БыстроеСохранениеToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveFile As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ИмяToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents ПарольToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents ИмяПарольToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ПередатьпринятьЖурналToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ИзменитьToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
End Class
