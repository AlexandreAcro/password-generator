﻿Public Class Prewiev
    Private Sub Prewiev_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Main.settings(1) <> "" And Main.settings(3) = True Then
            ListBox1.Visible = False
            Button1.Visible = False
            Button2.Visible = False
            Button3.Visible = False
            Button4.Visible = False
            Label1.Visible = True
            TextBox1.Visible = True
            Button5.Visible = True
            TextBox1.Text = ""
        Else
            ListBox1.Visible = True
            Button1.Visible = True
            Button2.Visible = True
            Button3.Visible = True
            Button4.Visible = True
            Label1.Visible = False
            TextBox1.Visible = False
            Button5.Visible = False
        End If
        If Main.settings(4) = "Redaction" Then
            Button2.Enabled = True
            Button3.Enabled = True
        Else
            Button2.Enabled = False
            Button3.Enabled = False
        End If
    End Sub
    Private Sub Prewiev_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        If Main.settings(1) = "" Or Main.settings(3) = False Then
            Renovate_ListBox()
        End If
    End Sub
    Public Sub Renovate_ListBox()
        ListBox1.Items.Clear()
        For i As Integer = 0 To Main.journal.Length - 1
            ListBox1.Items.Add(i + 1 & " - Имя: " & Main.journal(i).Split("/")(0) & ", Пароль: " & Main.journal(i).Split("/")(1))
        Next
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Search_Element.Close()
        Me.Close()
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Elements_Prewiev.ComboBox1.SelectedIndex = 0
        Elements_Prewiev.ShowDialog()
    End Sub
    Private Sub Prewiev_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Main.Show()
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If TextBox1.Text = Main.settings(1) Then
            ListBox1.Visible = True
            Button1.Visible = True
            Button2.Visible = True
            Button3.Visible = True
            Button4.Visible = True
            Label1.Visible = False
            TextBox1.Visible = False
            Button5.Visible = False
            Renovate_ListBox()
        Else
            TextBox1.Text = ""
            MsgBox("Пароль введён неверно!", , "Ошибка")
        End If
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Elements_Prewiev.ComboBox1.SelectedIndex = 1
        Elements_Prewiev.ShowDialog()
    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Search_Element.Show()
    End Sub
    Private Sub ContextMenuListBox_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ContextMenuListBox.Opening
        If ListBox1.SelectedIndex = -1 Then
            ContextMenuListBox.Items(0).Enabled = False
            ContextMenuListBox.Items(2).Enabled = False
            ContextMenuListBox.Items(4).Enabled = False
            ContextMenuListBox.Items(5).Enabled = False
            ContextMenuListBox.Items(6).Enabled = False
            ContextMenuListBox.Items(8).Enabled = False
            If Main.settings(4) = "Redaction" Then
                ContextMenuListBox.Items(8).Enabled = True
            Else
                ContextMenuListBox.Items(8).Enabled = False
            End If
        Else
            If Main.settings(4) = "Redaction" Then
                ContextMenuListBox.Items(0).Enabled = True
                ContextMenuListBox.Items(2).Enabled = True
                ContextMenuListBox.Items(8).Enabled = True
            Else
                ContextMenuListBox.Items(0).Enabled = False
                ContextMenuListBox.Items(2).Enabled = False
                ContextMenuListBox.Items(8).Enabled = False
            End If
            ContextMenuListBox.Items(4).Enabled = True
            ContextMenuListBox.Items(5).Enabled = True
            ContextMenuListBox.Items(6).Enabled = True
        End If
    End Sub
    Private Sub УдалитьToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles УдалитьToolStripMenuItem.Click
        Elements_Prewiev.ComboBox1.SelectedIndex = 1
        Elements_Prewiev.ShowDialog()
    End Sub
    Private Sub ИмяToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ИмяToolStripMenuItem.Click
        My.Computer.Clipboard.SetText(Main.journal(ListBox1.SelectedIndex).Split("/")(0))
    End Sub
    Private Sub ПарольToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ПарольToolStripMenuItem.Click
        My.Computer.Clipboard.SetText(Main.journal(ListBox1.SelectedIndex).Split("/")(1))
    End Sub
    Private Sub ИмяПарольToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ИмяПарольToolStripMenuItem.Click
        My.Computer.Clipboard.SetText("Имя: " & Main.journal(ListBox1.SelectedIndex).Split("/")(0) & ", Пароль: " & Main.journal(ListBox1.SelectedIndex).Split("/")(1))
    End Sub
    Private Sub ИмяToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ИмяToolStripMenuItem1.Click
        Try
            If My.Computer.FileSystem.FileExists(Main.settings(5)) Then
                Dim time_dat() As String = IO.File.ReadAllLines(Main.settings(5))
                Array.Resize(time_dat, time_dat.Length + 1)
                time_dat(time_dat.Length - 1) = Main.journal(ListBox1.SelectedIndex).Split("/")(0)
                IO.File.WriteAllLines(Main.settings(5), time_dat)
            Else
                My.Computer.FileSystem.WriteAllText(Main.settings(5), Main.journal(ListBox1.SelectedIndex).Split("/")(0), False)
            End If
            MsgBox("Сохранено завершено успешно!", , "Сообщение")
        Catch
            MsgBox("Сохранение не выполнено: нет доступа к файлу!", , "Ошибка")
        End Try
    End Sub
    Private Sub ПарольToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ПарольToolStripMenuItem1.Click
        Try
            If My.Computer.FileSystem.FileExists(Main.settings(5)) Then
                Dim time_dat() As String = IO.File.ReadAllLines(Main.settings(5))
                Array.Resize(time_dat, time_dat.Length + 1)
                time_dat(time_dat.Length - 1) = Main.journal(ListBox1.SelectedIndex).Split("/")(1)
                IO.File.WriteAllLines(Main.settings(5), time_dat)
            Else
                My.Computer.FileSystem.WriteAllText(Main.settings(5), Main.journal(ListBox1.SelectedIndex).Split("/")(1), False)
            End If
            MsgBox("Сохранено завершено успешно!", , "Сообщение")
        Catch
            MsgBox("Сохранение не выполнено: нет доступа к файлу!", , "Ошибка")
        End Try
    End Sub
    Private Sub ВсеToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ВсеToolStripMenuItem.Click
        Try
            If My.Computer.FileSystem.FileExists(Main.settings(5)) Then
                Dim time_dat() As String = IO.File.ReadAllLines(Main.settings(5))
                Array.Resize(time_dat, time_dat.Length + 1)
                time_dat(time_dat.Length - 1) = "Имя: " & Main.journal(ListBox1.SelectedIndex).Split("/")(0) & ", Пароль: " & Main.journal(ListBox1.SelectedIndex).Split("/")(1)
                IO.File.WriteAllLines(Main.settings(5), time_dat)
            Else
                My.Computer.FileSystem.WriteAllText(Main.settings(5), "Имя: " & Main.journal(ListBox1.SelectedIndex).Split("/")(0) & ", Пароль: " & Main.journal(ListBox1.SelectedIndex).Split("/")(1), False)
            End If
            MsgBox("Сохранено завершено успешно!", , "Сообщение")
        Catch
            MsgBox("Сохранение не выполнено: нет доступа к файлу!", , "Ошибка")
        End Try
    End Sub
    Private Sub ИмяПарольToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ИмяПарольToolStripMenuItem1.Click
        Dim result As DialogResult = SaveFile.ShowDialog
        If result = DialogResult.OK Then
            Try
                My.Computer.FileSystem.WriteAllText(SaveFile.FileName, "Имя: " & Main.journal(ListBox1.SelectedIndex).Split("/")(0) & ", Пароль: " & Main.journal(ListBox1.SelectedIndex).Split("/")(1), False)
                MsgBox("Сохранено завершено успешно!", , "Сообщение")
            Catch
                MsgBox("Сохранение не выполнено: нет доступа к файлу!", , "Ошибка")
            End Try
        End If
    End Sub
    Private Sub ИмяToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles ИмяToolStripMenuItem2.Click
        Try
            My.Computer.FileSystem.WriteAllText(SaveFile.FileName, Main.journal(ListBox1.SelectedIndex).Split("/")(0), False)
            MsgBox("Сохранено завершено успешно!", , "Сообщение")
        Catch
            MsgBox("Сохранение не выполнено: нет доступа к файлу!", , "Ошибка")
        End Try
    End Sub
    Private Sub ПарольToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles ПарольToolStripMenuItem2.Click
        Try
            My.Computer.FileSystem.WriteAllText(SaveFile.FileName, Main.journal(ListBox1.SelectedIndex).Split("/")(1), False)
            MsgBox("Сохранено завершено успешно!", , "Сообщение")
        Catch
            MsgBox("Сохранение не выполнено: нет доступа к файлу!", , "Ошибка")
        End Try
    End Sub
    Private Sub ПередатьпринятьЖурналToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ПередатьпринятьЖурналToolStripMenuItem.Click
        SendJournal.ShowDialog()
    End Sub
    Private Sub ИзменитьToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ИзменитьToolStripMenuItem.Click
        Elements_Prewiev.ComboBox1.SelectedIndex = 2
        Elements_Prewiev.ShowDialog()
    End Sub
End Class
