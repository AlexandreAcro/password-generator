﻿Public Class Search_Element
    Dim pass_list(-1) As String
    Dim name_list(-1) As String
    Dim num_list(-1) As Integer
    Private Sub Search_Element_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Prewiev.Left + Prewiev.Width + Me.Width + 10 > My.Computer.Screen.Bounds.Width Then
            If Prewiev.Left - Me.Width < 0 Then
                Me.Left = Int(Prewiev.Left + Prewiev.Width / 2 - Me.Width / 2)
            Else
                Me.Left = Prewiev.Left - Me.Width - 20
            End If
        Else
            Me.Left = Prewiev.Left + Prewiev.Width + 20
        End If
        Me.Top = Int(Prewiev.Top + Prewiev.Height / 2 - Me.Height / 2)
    End Sub
    Private Sub Search_Element_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        ComboBox1.SelectedIndex = 0
        TextBox1.Text = ""
        CheckBox1.Enabled = True
        CheckBox2.Enabled = True
        CheckBox1.Checked = False
        CheckBox2.Checked = False
        RadioButton2.Checked = True
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If TextBox1.Text = "" Then
            MsgBox("Поле для поиска пустое!", , "Ошибка")
            Exit Sub
        End If
        ComboBox1.Enabled = False
        UseWaitCursor = True
        CheckBox1.Enabled = False
        CheckBox2.Enabled = False
        ListBox1.Items.Clear()
        Array.Resize(pass_list, 0)
        Array.Resize(name_list, 0)
        Array.Resize(num_list, 0)
        If ComboBox1.SelectedIndex = 0 Then
            If CheckBox2.Checked Then
                If CheckBox1.Checked Then
                    For i As Integer = 0 To Main.journal.Length - 1
                        Dim time_list() As String = Main.journal(i).Split("/")
                        If time_list(0) = TextBox1.Text Then
                            Array.Resize(pass_list, pass_list.Length + 1)
                            pass_list(pass_list.Length - 1) = time_list(1)
                            Array.Resize(name_list, name_list.Length + 1)
                            name_list(name_list.Length - 1) = time_list(0)
                            Array.Resize(num_list, num_list.Length + 1)
                            num_list(num_list.Length - 1) = i
                        End If
                    Next
                Else
                    For i As Integer = 0 To Main.journal.Length - 1
                        Dim time_list() As String = Main.journal(i).Split("/")
                        If time_list(0).ToLower = TextBox1.Text.ToLower Then
                            Array.Resize(pass_list, pass_list.Length + 1)
                            pass_list(pass_list.Length - 1) = time_list(1)
                            Array.Resize(name_list, name_list.Length + 1)
                            name_list(name_list.Length - 1) = time_list(0)
                            Array.Resize(num_list, num_list.Length + 1)
                            num_list(num_list.Length - 1) = i
                        End If
                    Next
                End If
            Else
                If CheckBox1.Checked Then
                    If RadioButton2.Checked Then
                        For i As Integer = 0 To Main.journal.Length - 1
                            Dim time_list() As String = Main.journal(i).Split("/")
                            If time_list(0).IndexOf(TextBox1.Text) > -1 Then
                                Array.Resize(pass_list, pass_list.Length + 1)
                                pass_list(pass_list.Length - 1) = time_list(1)
                                Array.Resize(name_list, name_list.Length + 1)
                                name_list(name_list.Length - 1) = time_list(0)
                                Array.Resize(num_list, num_list.Length + 1)
                                num_list(num_list.Length - 1) = i
                            End If
                        Next
                    Else
                        For i As Integer = 0 To Main.journal.Length - 1
                            Dim time_list() As String = Main.journal(i).Split("/")
                            If TextBox1.Text.IndexOf(time_list(0)) > -1 Then
                                Array.Resize(pass_list, pass_list.Length + 1)
                                pass_list(pass_list.Length - 1) = time_list(1)
                                Array.Resize(name_list, name_list.Length + 1)
                                name_list(name_list.Length - 1) = time_list(0)
                                Array.Resize(num_list, num_list.Length + 1)
                                num_list(num_list.Length - 1) = i
                            End If
                        Next
                    End If
                Else
                    If RadioButton2.Checked Then
                        For i As Integer = 0 To Main.journal.Length - 1
                            Dim time_list() As String = Main.journal(i).Split("/")
                            If time_list(0).ToLower.IndexOf(TextBox1.Text.ToLower) > -1 Then
                                Array.Resize(pass_list, pass_list.Length + 1)
                                pass_list(pass_list.Length - 1) = time_list(1)
                                Array.Resize(name_list, name_list.Length + 1)
                                name_list(name_list.Length - 1) = time_list(0)
                                Array.Resize(num_list, num_list.Length + 1)
                                num_list(num_list.Length - 1) = i
                            End If
                        Next
                    Else
                        For i As Integer = 0 To Main.journal.Length - 1
                            Dim time_list() As String = Main.journal(i).Split("/")
                            If TextBox1.Text.ToLower.IndexOf(time_list(0).ToLower) > -1 Then
                                Array.Resize(pass_list, pass_list.Length + 1)
                                pass_list(pass_list.Length - 1) = time_list(1)
                                Array.Resize(name_list, name_list.Length + 1)
                                name_list(name_list.Length - 1) = time_list(0)
                                Array.Resize(num_list, num_list.Length + 1)
                                num_list(num_list.Length - 1) = i
                            End If
                        Next
                    End If
                End If
            End If
            If name_list.Length = 0 Then
                If CheckBox2.Checked Then
                    If CheckBox1.Checked Then
                        MsgBox("Поиск не дал результатов." & Chr(10) & "    Совет: отключите опцию 'Поиск по целой фразе' и(или) опцию 'Утчитывать регистр'.", , "Сообщение")
                    Else
                        MsgBox("Поиск не дал результатов." & Chr(10) & "    Совет: отключите опцию 'Поиск по целой фразе'.", , "Сообщение")
                    End If
                Else
                    If CheckBox1.Checked Then
                        MsgBox("Поиск не дал результатов." & Chr(10) & "    Совет: отключите опцию 'Утчитывать регистр'.", , "Сообщение")
                    Else
                        MsgBox("Поиск не дал результатов.", , "Сообщение")
                    End If
                End If
            Else
                For i As Integer = 0 To name_list.Length - 1
                    ListBox1.Items.Add(num_list(i) + 1 & " - Имя: " & name_list(i) & ", Пароль: " & pass_list(i))
                Next
                MsgBox("Поиск завершён успешно!" & Chr(10) & "Найдено: " & name_list.Length & " элементов.", , "Сообщение")
            End If
        ElseIf ComboBox1.SelectedIndex = 1 Then
            If CheckBox2.Checked Then
                For i As Integer = 0 To Main.journal.Length - 1
                    Dim time_list() As String = Main.journal(i).Split("/")
                    If time_list(1) = TextBox1.Text Then
                        Array.Resize(pass_list, pass_list.Length + 1)
                        pass_list(pass_list.Length - 1) = time_list(1)
                        Array.Resize(name_list, name_list.Length + 1)
                        name_list(name_list.Length - 1) = time_list(0)
                        Array.Resize(num_list, num_list.Length + 1)
                        num_list(num_list.Length - 1) = i
                    End If
                Next
            Else
                If RadioButton2.Checked Then
                    For i As Integer = 0 To Main.journal.Length - 1
                        Dim time_list() As String = Main.journal(i).Split("/")
                        If time_list(1).IndexOf(TextBox1.Text) > -1 Then
                            Array.Resize(pass_list, pass_list.Length + 1)
                            pass_list(pass_list.Length - 1) = time_list(1)
                            Array.Resize(name_list, name_list.Length + 1)
                            name_list(name_list.Length - 1) = time_list(0)
                            Array.Resize(num_list, num_list.Length + 1)
                            num_list(num_list.Length - 1) = i
                        End If
                    Next
                Else
                    For i As Integer = 0 To Main.journal.Length - 1
                        Dim time_list() As String = Main.journal(i).Split("/")
                        If TextBox1.Text.IndexOf(time_list(1)) > -1 Then
                            Array.Resize(pass_list, pass_list.Length + 1)
                            pass_list(pass_list.Length - 1) = time_list(1)
                            Array.Resize(name_list, name_list.Length + 1)
                            name_list(name_list.Length - 1) = time_list(0)
                            Array.Resize(num_list, num_list.Length + 1)
                            num_list(num_list.Length - 1) = i
                        End If
                    Next
                End If
            End If
            If name_list.Length = 0 Then
                If CheckBox2.Checked Then
                    MsgBox("Поиск не дал результатов." & Chr(10) & "    Совет: отключите опцию 'Поиск по целой фразе'.", , "Сообщение")
                Else
                    MsgBox("Поиск не дал результатов.", , "Сообщение")
                End If
            Else
                For i As Integer = 0 To name_list.Length - 1
                    ListBox1.Items.Add(num_list(i) + 1 & " - Имя: " & name_list(i) & ", Пароль: " & pass_list(i))
                Next
                MsgBox("Поиск завершён успешно!" & Chr(10) & "Найдено: " & name_list.Length & " элементов.", , "Сообщение")
            End If
        End If
        ComboBox1.Enabled = True
        UseWaitCursor = False
        If ComboBox1.SelectedIndex = 0 Then
            CheckBox1.Enabled = True
            CheckBox2.Enabled = True
        ElseIf ComboBox1.SelectedIndex = 1 Then
            CheckBox1.Enabled = False
            CheckBox2.Enabled = True
        End If
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.SelectedIndex = 0 Then
            CheckBox1.Enabled = True
            CheckBox2.Enabled = True
            CheckBox1.Checked = False
            CheckBox2.Checked = False
        ElseIf ComboBox1.SelectedIndex = 1 Then
            CheckBox1.Enabled = False
            CheckBox2.Enabled = True
            CheckBox1.Checked = True
            CheckBox2.Checked = False
        End If
    End Sub
    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged
        If ListBox1.SelectedIndex > -1 Then
            If Main.journal(num_list(ListBox1.SelectedIndex)).Split("/")(0) = name_list(ListBox1.SelectedIndex) And Main.journal(num_list(ListBox1.SelectedIndex)).Split("/")(1) = pass_list(ListBox1.SelectedIndex) And Main.journal.Length > num_list(ListBox1.SelectedIndex) Then
                Prewiev.ListBox1.SelectedIndex = num_list(ListBox1.SelectedIndex)
            Else
                MsgBox("Элемент не найден!", , "Сообщение")
            End If
        End If
    End Sub
    Dim animation_mode As Short = 1
    Private Sub F_IO_Animation_Tick(sender As Object, e As EventArgs) Handles F_IO_Animation.Tick
        If animation_mode = 1 Then
            If Label4.Left = 12 Then
                F_IO_Animation.Stop()
            Else
                Label4.Left += 20
            End If
        ElseIf animation_mode = -1 Then
            If Label4.Left <= -308 Then
                F_IO_Animation.Stop()
            Else
                Label4.Left -= 20
            End If
        End If
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If animation_mode = 1 Then
            Button2.Text = "=>"
            animation_mode = -1
            F_IO_Animation.Start()
        ElseIf animation_mode = -1 Then
            Button2.Text = "<="
            animation_mode = 1
            F_IO_Animation.Start()
        End If
    End Sub
    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked Then
            RadioButton1.Enabled = False
            RadioButton2.Enabled = False
        Else
            RadioButton1.Enabled = True
            RadioButton2.Enabled = True
        End If
    End Sub
End Class
