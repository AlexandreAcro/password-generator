﻿Public Class SendJournal
    Dim file As String = ""
    Private Sub SendJournal_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        ComboBox1.SelectedIndex = 0
        ProgressBar1.Value = 0
        RadioButton1.Enabled = True
        RadioButton2.Enabled = True
        RadioButton1.Checked = True
        TextBox2.Enabled = False
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub
    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        TextBox2.Enabled = False
    End Sub
    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        TextBox2.Enabled = True
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.SelectedIndex = 0 Then
            Label2.Text = "Файл передаваемого журнала:"
            RadioButton1.Enabled = True
            RadioButton2.Enabled = True
            If RadioButton2.Checked Then
                TextBox2.Enabled = True
            Else
                TextBox2.Enabled = False
            End If
            file = ""
            TextBox1.Text = ""
        Else
            Label2.Text = "Файл принимаемого журнала:"
            RadioButton1.Enabled = False
            RadioButton2.Enabled = False
            TextBox2.Enabled = False
            file = ""
            TextBox1.Text = ""
        End If
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If ComboBox1.SelectedIndex = 0 Then
            Dim result As DialogResult = SaveFile.ShowDialog
            If result = DialogResult.OK Then
                file = SaveFile.FileName
            End If

        Else
            Dim result As DialogResult = OpenFile.ShowDialog
            If result = DialogResult.OK Then
                file = OpenFile.FileName
            End If
        End If
        TextBox1.Text = file
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Close()
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If TextBox1.Text = "" Then
            MsgBox("Вы не задали путь к журналу!", MsgBoxStyle.OkOnly, "Ошибка")
            Exit Sub
        End If
        If ComboBox1.SelectedIndex = 0 Then
            Dim err_pos As Short = 0
            Try
                Button2.Enabled = False
                Button3.Enabled = False
                Dim result As MsgBoxResult = MsgBox("    Внимание!    " & Chr(10) & "Файл сохраненного для передачи журнала не защищен! Перемещайте этот файл осторожно, не оставляя копий! После приёма файла журнала, его рекомендуется удалить." & Chr(10) & "Вы действительно хотите сохранить журнал для передачи?", MsgBoxStyle.YesNo, "Сообщение")
                If result = MsgBoxResult.Yes Then
                    err_pos = 1
                    If RadioButton1.Checked Then
                        Dim send_journal(Main.journal.Length - 1) As String
                        Dim prog(1) As Integer
                        If send_journal.Length < 100 Then
                            prog(0) = 0
                            prog(1) = Math.Round(100 / send_journal.Length)
                        Else
                            prog(0) = 1
                            prog(1) = Math.Round(send_journal.Length / 100)
                        End If
                        For i As Integer = 0 To send_journal.Length - 1
                            send_journal(i) = "/N:" & Main.journal(i).Split("/")(0) & "/P:" & Main.journal(i).Split("/")(1)
                            If prog(0) = 0 Then
                                If ProgressBar1.Value + prog(1) <= 100 Then
                                    ProgressBar1.Value += prog(1)
                                Else
                                    ProgressBar1.Value = 100
                                End If
                            Else
                                If Math.IEEERemainder(i, prog(1)) = 0 Then
                                    If ProgressBar1.Value + 1 < 100 Then
                                        ProgressBar1.Value += 1
                                    Else
                                        ProgressBar1.Value = 100
                                    End If
                                End If
                            End If
                        Next
                        IO.File.WriteAllLines(file, send_journal)
                        ProgressBar1.Value = 100
                    Else
                        Dim send_journal(-1) As String
                        If TextBox2.Text.IndexOf(",") > -1 Then
                            Dim numbers() As String = TextBox2.Text.Split(","), number_povtor(-1) As Integer
                            Dim prog(1) As Integer
                            If numbers.Length < 100 Then
                                prog(0) = 0
                                prog(1) = Math.Round(100 / numbers.Length)
                            Else
                                prog(0) = 1
                                prog(1) = Math.Round(numbers.Length / 100)
                            End If
                            For i As Integer = 0 To numbers.Length - 1
                                Dim test As Integer = numbers(i)
                                If Main.journal.Length < test Or test <= 0 Then Error 1
                                Dim num_have As Boolean = False
                                For i1 As Integer = 0 To number_povtor.Length - 1
                                    If number_povtor(i1) = test Then
                                        num_have = True
                                    End If
                                Next
                                If num_have = True Then Error 1
                                Array.Resize(number_povtor, number_povtor.Length + 1)
                                number_povtor(number_povtor.Length - 1) = test
                                If prog(0) = 0 Then
                                    If ProgressBar1.Value + prog(1) <= 80 Then
                                        ProgressBar1.Value += prog(1)
                                    Else
                                        ProgressBar1.Value = 80
                                    End If
                                Else
                                    If Math.IEEERemainder(i, prog(1)) = 0 Then
                                        If ProgressBar1.Value + 1 < 80 Then
                                            ProgressBar1.Value += 1
                                        Else
                                            ProgressBar1.Value = 80
                                        End If
                                    End If
                                End If
                            Next
                            Array.Resize(send_journal, numbers.Length)
                            For i As Integer = 0 To numbers.Length - 1
                                send_journal(i) = "/N:" & Main.journal(numbers(i) - 1).Split("/")(0) & "/P:" & Main.journal(numbers(i) - 1).Split("/")(1)
                                If prog(0) = 0 Then
                                    If ProgressBar1.Value + prog(1) <= 100 Then
                                        ProgressBar1.Value += prog(1)
                                    Else
                                        ProgressBar1.Value = 100
                                    End If
                                Else
                                    If Math.IEEERemainder(i, prog(1)) = 0 Then
                                        If ProgressBar1.Value + 1 < 100 Then
                                            ProgressBar1.Value += 1
                                        Else
                                            ProgressBar1.Value = 100
                                        End If
                                    End If
                                End If
                            Next
                            ProgressBar1.Value = 100
                        ElseIf TextBox2.Text.IndexOf("-") > -1 Then
                            Dim interval() As String = TextBox2.Text.Split("-")
                            Dim prog(1) As Integer
                            If interval(1) - interval(0) < 98 Then
                                prog(0) = 0
                                prog(1) = Math.Round(98 / (interval(1) - interval(0)))
                            Else
                                prog(0) = 1
                                prog(1) = Math.Round((interval(1) - interval(0)) / 98)
                            End If
                            If interval.Length > 2 Then Error 1
                            Dim test As Integer = interval(0)
                            test = interval(1)
                            If Main.journal.Length < interval(1) Or interval(0) <= 0 Or interval(1) - interval(0) <= 0 Then Error 1
                            ProgressBar1.Value = 2
                            For i As Integer = interval(0) - 1 To interval(1) - 1
                                Array.Resize(send_journal, send_journal.Length + 1)
                                send_journal(send_journal.Length - 1) = "/N:" & Main.journal(i).Split("/")(0) & "/P:" & Main.journal(i).Split("/")(1)
                                If prog(0) = 0 Then
                                    If ProgressBar1.Value + prog(1) <= 100 Then
                                        ProgressBar1.Value += prog(1)
                                    Else
                                        ProgressBar1.Value = 100
                                    End If
                                Else
                                    If Math.IEEERemainder(i, prog(1)) = 0 Then
                                        If ProgressBar1.Value + 1 < 100 Then
                                            ProgressBar1.Value += 1
                                        Else
                                            ProgressBar1.Value = 100
                                        End If
                                    End If
                                End If
                            Next
                        Else
                            Dim test As Integer = TextBox2.Text
                            If Main.journal.Length < test Or test <= 0 Then Error 1
                            Array.Resize(send_journal, 1)
                            send_journal(0) = "/N:" & Main.journal(test - 1).Split("/")(0) & "/P:" & Main.journal(test - 1).Split("/")(1)
                            ProgressBar1.Value = 100
                        End If
                        IO.File.WriteAllLines(file, send_journal)
                    End If
                End If
                MsgBox("Журнал сохренен успешно!", MsgBoxStyle.OkOnly, "Сообщение")
                ProgressBar1.Value = 0
            Catch
                If err_pos = 0 Then
                    MsgBox("Неправильно заданы параметры работы в данном режиме!", MsgBoxStyle.OkOnly, "Ошибка")
                Else
                    MsgBox("Нет доступа к указанному файлу!", MsgBoxStyle.OkOnly, "Ошибка")
                End If
                ProgressBar1.Value = 0
            End Try
            Button2.Enabled = True
            Button3.Enabled = True
        Else
            Dim result As MsgBoxResult = MsgBox("Вы действительно хотите добавить этот файл в журнал?", MsgBoxStyle.YesNo, "Сообщение")
            If result = MsgBoxResult.Yes Then
                Button2.Enabled = False
                Button3.Enabled = False
                Try
                    Dim get_journal() As String = IO.File.ReadAllLines(file), def_sel As Short = 0
                    Dim prog(1) As Integer
                    If get_journal.Length < 98 Then
                        prog(0) = 0
                        prog(1) = Math.Round(98 / get_journal.Length)
                    Else
                        prog(0) = 1
                        prog(1) = Math.Round(get_journal.Length / 98)
                    End If
                    For i As Integer = 0 To get_journal.Length - 1
                        Dim name_pass() As String = get_journal(i).Split("/")
                        If name_pass.Length > 3 Or name_pass.Length < 3 Then Error 1
                        If name_pass(0) <> "" Then Error 1
                        If Mid(name_pass(1), 1, 2) <> "N:" Or Mid(name_pass(2), 1, 2) <> "P:" Then Error 1
                        Dim time_dat As String = Mid(name_pass(1), 3)
                        If time_dat = "" Or time_dat.IndexOf("<") > -1 Or time_dat.IndexOf(">") > -1 Or time_dat.IndexOf("/") > -1 Or time_dat.IndexOf("\") > -1 Then Error 1
                        time_dat = Mid(name_pass(2), 3)
                            If time_dat.IndexOf(" ") > -1 Then Error 1
                            For i1 As Integer = 1 To Main.Koding(True, time_dat).ToString.Length Step 9
                            If i1 + 9 <= Main.Koding(True, time_dat).ToString.Length Then
                                Dim time As Integer = Mid(Main.Koding(True, time_dat), i1, 9)
                            Else
                                Dim time As Integer = Mid(Main.Koding(True, time_dat), i1)
                            End If
                        Next
                        Dim p_had As Boolean = False, pos As Integer, name_pass_r As String = Mid(name_pass(1), 3)
                        For i1 As Integer = 0 To Main.journal.Length - 1
                            If Main.journal(i1).Split("/")(0) = name_pass_r Then
                                p_had = True
                                pos = i1
                                Exit For
                            End If
                        Next
                        If p_had = False Then
                            Array.Resize(Main.journal, Main.journal.Length + 1)
                            Main.journal(Main.journal.Length - 1) = name_pass_r & "/" & Mid(name_pass(2), 3)
                        Else
                            If def_sel = 0 Then
                                DoubleNames.TextBox1.Text = name_pass_r
                                Dim result1 As DialogResult = DoubleNames.ShowDialog()
                                If result1 = DialogResult.Abort Then
                                    If DoubleNames.CheckBox1.Checked Then
                                        def_sel = 1
                                    End If
                                    Main.journal(pos) = name_pass_r & "/" & Mid(name_pass(2), 3)
                                ElseIf result1 = DialogResult.Ignore Then
                                    If DoubleNames.CheckBox1.Checked Then
                                        def_sel = 2
                                    End If
                                ElseIf result1 = DialogResult.Retry Then
                                    If DoubleNames.CheckBox1.Checked Then
                                        def_sel = 3
                                    End If
                                    Dim name_perf As Integer = 1
                                    p_had = False
                                    While p_had <> True
                                        For i1 As Integer = 0 To Main.journal.Length - 1
                                            If Main.journal(i1).Split("/")(0) = name_pass_r & "-" & name_perf Then
                                                p_had = True
                                                pos = i1
                                                Exit For
                                            End If
                                        Next
                                        If p_had = True Then
                                            p_had = False
                                            name_perf += 1
                                        Else
                                            p_had = True
                                        End If
                                    End While
                                    Array.Resize(Main.journal, Main.journal.Length + 1)
                                    Main.journal(Main.journal.Length - 1) = name_pass_r & "-" & name_perf & "/" & Mid(name_pass(2), 3)
                                End If
                            ElseIf def_sel = 1 Then
                                Main.journal(pos) = name_pass_r & "/" & Mid(name_pass(2), 3)
                            ElseIf def_sel = 3 Then
                                Dim name_perf As Integer = 1, j_element_name As String = Main.journal(pos).Split("/")(0)
                                p_had = False
                                While p_had <> True
                                    For i1 As Integer = 0 To Main.journal.Length - 1
                                        If Main.journal(i1).Split("/")(0) = name_pass_r & "-" & name_perf Then
                                            p_had = True
                                            pos = i1
                                            Exit For
                                        End If
                                    Next
                                    If p_had = True Then
                                        p_had = False
                                        name_perf += 1
                                    Else
                                        p_had = True
                                    End If
                                End While
                                Array.Resize(Main.journal, Main.journal.Length + 1)
                                Main.journal(Main.journal.Length - 1) = name_pass_r & "-" & name_perf & "/" & Mid(name_pass(2), 3)
                            End If
                        End If
                        If prog(0) = 0 Then
                            If ProgressBar1.Value + prog(1) <= 98 Then
                                ProgressBar1.Value += prog(1)
                            Else
                                ProgressBar1.Value = 98
                            End If
                        Else
                            If Math.IEEERemainder(i, prog(1)) = 0 Then
                                If ProgressBar1.Value + 1 < 98 Then
                                    ProgressBar1.Value += 1
                                Else
                                    ProgressBar1.Value = 98
                                End If
                            End If
                        End If
                    Next
                    ProgressBar1.Value = 100
                    Main.Journal_Write()
                    Prewiev.Renovate_ListBox()
                    MsgBox("Журнал добавлен успешно!", MsgBoxStyle.OkOnly, "Сообщение")
                    ProgressBar1.Value = 0
                Catch
                    MsgBox("Добавляемый журнал повреждён или(и) имеет неправильный формат!", MsgBoxStyle.OkOnly, "Ошибка")
                    ProgressBar1.Value = 0
                End Try
                Button2.Enabled = True
                Button3.Enabled = True
            End If
        End If
    End Sub
End Class
