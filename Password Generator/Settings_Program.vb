﻿Public Class Settings_Program
    Private Sub Main_Settings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ComboBox1.Items.Clear()
        For Each time_dat As String In Main.libs
            Dim name As String = My.Computer.FileSystem.GetFileInfo(time_dat).Name
            If name.Length > 29 Then
                ComboBox1.Items.Add(Mid(name, 1, 24))
            Else
                ComboBox1.Items.Add(Mid(name, 1, name.Length - 5))
            End If
        Next
        If My.Computer.FileSystem.FileExists(Main.settings(0)) = False Then
            Main.settings(0) = Main.way & "/Generate Libraries/GPSL.glib"
            If My.Computer.FileSystem.FileExists(Application.StartupPath & "/Generate Libraries/GPSL.glib") = False Then
                My.Computer.FileSystem.WriteAllBytes(Application.StartupPath & "/Generate Libraries/GPSL.glib", My.Resources.GPSL, False)
            End If
        End If
        Dim name_select_file As String = My.Computer.FileSystem.GetFileInfo(Main.settings(0)).Name
        If name_select_file.Length > 29 Then
            ComboBox1.SelectedItem = Mid(name_select_file, 1, 24)
        Else
            ComboBox1.SelectedItem = Mid(name_select_file, 1, name_select_file.Length - 5)
        End If
        If Main.settings(2) = True Then
            CheckBox1.Checked = True
        Else
            CheckBox1.Checked = False
        End If
        If Main.settings(3) = True Then
            CheckBox3.Checked = True
        Else
            CheckBox3.Checked = False
        End If
        If Main.settings(4) = "Redaction" Then
            ComboBox2.SelectedIndex = 0
        Else
            ComboBox2.SelectedIndex = 1
        End If
        CheckBox2.Checked = False
        TextBox2.Text = ""
        TextBox1.Text = Main.settings(5)
        If Main.settings(1) <> "" Then
            TextBox2.Enabled = True
            TextBox3.Enabled = False
            TextBox4.Enabled = False
            CheckBox1.Enabled = False
            CheckBox2.Enabled = False
            CheckBox3.Enabled = False
            ComboBox2.Enabled = False
            TextBox3.Text = Main.settings(1)
            TextBox4.Text = Main.settings(1)
        Else
            TextBox2.Enabled = False
            TextBox3.Enabled = True
            TextBox4.Enabled = True
            CheckBox1.Enabled = True
            CheckBox2.Enabled = True
            CheckBox3.Enabled = True
            ComboBox2.Enabled = True
            TextBox3.Text = ""
            TextBox4.Text = ""
        End If
        SaveFile.FileName = Main.settings(5)
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim result As DialogResult = SaveFile.ShowDialog()
        If result = DialogResult.OK Then
            TextBox1.Text = SaveFile.FileName
        End If
    End Sub
    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked Then
            TextBox4.Visible = False
            Label6.Visible = False
            TextBox3.PasswordChar = ""
        Else
            TextBox4.Visible = True
            Label6.Visible = True
            TextBox3.PasswordChar = "*"
        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            If CheckBox2.Checked Then
                If TextBox3.Text <> "" Then
                    For i As Integer = 1 To Main.Koding(True, TextBox3.Text).ToString.Length Step 9
                        If i + 9 <= Main.Koding(True, TextBox3.Text).ToString.Length Then
                            Dim time As Integer = Mid(Main.Koding(True, TextBox3.Text), i, 9)
                        Else
                            Dim time As Integer = Mid(Main.Koding(True, TextBox3.Text), i)
                        End If
                    Next
                End If
                Main.settings(1) = TextBox3.Text
            Else
                If TextBox3.Text <> "" And TextBox4.Text <> "" Then
                    For i As Integer = 1 To Main.Koding(True, TextBox3.Text).ToString.Length Step 9
                        If i + 9 <= Main.Koding(True, TextBox3.Text).ToString.Length Then
                            Dim time As Integer = Mid(Main.Koding(True, TextBox3.Text), i, 9)
                        Else
                            Dim time As Integer = Mid(Main.Koding(True, TextBox3.Text), i)
                        End If
                    Next
                End If
                If TextBox3.Text = TextBox4.Text Then
                    Main.settings(1) = TextBox3.Text
                Else
                    Label10.Visible = True
                    Exit Sub
                End If
            End If
        Catch
            MsgBox("Пароль содержит недопустимые символы!", , "Ошибка")
            Exit Sub
        End Try
        Label10.Visible = False
        Main.settings(0) = Main.libs(ComboBox1.SelectedIndex)
        Main.settings(2) = CheckBox1.Checked
        Main.settings(3) = CheckBox3.Checked
        If ComboBox2.SelectedIndex = 0 Then
            Main.settings(4) = "Redaction"
        Else
            Main.settings(4) = "Prewiev"
        End If
        Main.settings(5) = TextBox1.Text
        Main.Settings_Write()
        Me.Close()
    End Sub
    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged
        If TextBox2.Text = Main.settings(1) Then
            TextBox3.Enabled = True
            TextBox4.Enabled = True
            CheckBox1.Enabled = True
            CheckBox2.Enabled = True
            CheckBox3.Enabled = True
            ComboBox2.Enabled = True
        Else
            TextBox3.Enabled = False
            TextBox4.Enabled = False
            CheckBox1.Enabled = False
            CheckBox2.Enabled = False
            CheckBox3.Enabled = False
            ComboBox2.Enabled = False
            TextBox3.Text = Main.settings(1)
            TextBox4.Text = Main.settings(1)
            CheckBox2.Checked = False
        End If
    End Sub
    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Try
            Dim data_time() As String = IO.File.ReadAllLines(Main.libs(ComboBox1.SelectedIndex)), end_while As Boolean = False, time_position As Integer = 0, position As Integer = 0, info(4) As String
            While end_while <> True
                If position = data_time.Length Then
                    end_while = True
                    Error 1
                ElseIf data_time(position) = "[INFORMATION]" Then
                    position += 1
                    end_while = True
                Else
                    position += 1
                End If
            End While
            time_position = position
            end_while = False
            While end_while <> True
                If position = data_time.Length Then
                    Error 1
                ElseIf data_time(position).IndexOf("[") = 0 Then
                    Error 1
                ElseIf Mid(data_time(position), 1, 8) = "version=" Then
                    end_while = True
                Else
                    position += 1
                End If
            End While
            info(0) = Mid(data_time(position), 9)
            position = time_position
            end_while = False
            While end_while <> True
                If position = data_time.Length Then
                    Error 1
                ElseIf data_time(position).IndexOf("[") = 0 Then
                    Error 1
                ElseIf Mid(data_time(position), 1, 8) = "authors=" Then
                    end_while = True
                Else
                    position += 1
                End If
            End While
            info(1) = Mid(data_time(position), 9)
            position = time_position
            end_while = False
            While end_while <> True
                If position = data_time.Length Then
                    Error 1
                ElseIf data_time(position).IndexOf("[") = 0 Then
                    Error 1
                ElseIf Mid(data_time(position), 1, 5) = "info=" Then
                    end_while = True
                Else
                    position += 1
                End If
            End While
            info(4) = Mid(data_time(position), 6)
            time_position = 0
            position = 0
            end_while = False
            While end_while <> True
                If position = data_time.Length Then
                    end_while = True
                    Error 1
                ElseIf data_time(position) = "[LIVELS]" Then
                    position += 1
                    end_while = True
                Else
                    position += 1
                End If
            End While
            time_position = position
            end_while = False
            While end_while <> True
                If position = data_time.Length Then
                    Error 1
                ElseIf data_time(position).IndexOf("[") = 0 Then
                    Error 1
                ElseIf Mid(data_time(position), 1, 7) = "livels=" Then
                    end_while = True
                Else
                    position += 1
                End If
            End While
            info(2) = Mid(data_time(position), 8)
            position = time_position
            end_while = False
            While end_while <> True
                If position = data_time.Length Then
                    Error 1
                ElseIf data_time(position).IndexOf("[") = 0 Then
                    Error 1
                ElseIf Mid(data_time(position), 1, 8) = "simvols=" Then
                    end_while = True
                Else
                    position += 1
                End If
            End While
            info(3) = Mid(data_time(position), 9)
            MsgBox("Информация: " & Chr(10) & "  Описание: " & info(4) & Chr(10) & "  Версия: " & info(0) & Chr(10) & "  Авторы: " & info(1) & Chr(10) & "  Генерационных уровней: " & info(2) & Chr(10) & "  Символов для генерации: " & info(3), , "Информация")
        Catch
            Dim time_dat As String = My.Computer.FileSystem.GetFileInfo(Main.libs(ComboBox1.SelectedIndex)).Name
            If time_dat.Length > 29 Then
                MsgBox("Библиотека генерации '" & Mid(time_dat, 1, 24) & "' неисправна! Совет: удалите эту библиотеку.", , "Ошибка")
            Else
                MsgBox("Библиотека генерации '" & Mid(time_dat, 1, time_dat.Length - 5) & "' неисправна! Совет: удалите эту библиотеку.", , "Ошибка")
            End If
            Dim name_select_file As String = My.Computer.FileSystem.GetFileInfo(Main.settings(0)).Name
            If name_select_file.Length > 29 Then
                ComboBox1.SelectedItem = Mid(name_select_file, 1, 24)
            Else
                ComboBox1.SelectedItem = Mid(name_select_file, 1, name_select_file.Length - 5)
            End If
        End Try
    End Sub
    Private Sub ВставитьToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ВставитьToolStripMenuItem.Click
        If ActiveControl.Name = "TextBox2" Then
            If My.Computer.Clipboard.ContainsText Then
                Dim coursor_position As Integer = TextBox2.SelectionStart
                TextBox2.Text &= My.Computer.Clipboard.GetText
                TextBox2.SelectionStart = coursor_position + My.Computer.Clipboard.GetText.Length
            End If
        ElseIf ActiveControl.Name = "TextBox3" Then
            If My.Computer.Clipboard.ContainsText Then
                Dim coursor_position As Integer = TextBox3.SelectionStart
                TextBox3.Text &= My.Computer.Clipboard.GetText
                TextBox3.SelectionStart = coursor_position + My.Computer.Clipboard.GetText.Length
            End If
        ElseIf ActiveControl.Name = "TextBox4" Then
            If My.Computer.Clipboard.ContainsText Then
                Dim coursor_position As Integer = TextBox4.SelectionStart
                TextBox4.Text &= My.Computer.Clipboard.GetText
                TextBox4.SelectionStart = coursor_position + My.Computer.Clipboard.GetText.Length
            End If
        End If
    End Sub
End Class
